 # LyGo Open Document (Word, Excel)
 
 Uses [UNIOFFICE](https://github.com/unidoc/unioffice) implementation.
 
 With lygo_opendoc you can:
 * Create reports using .docx as templates with "mustache" fields
 
 ## How to Use
 
 To use just call:
 
 `go get -u bitbucket.org/lygo/lygo_ex_opendoc`
 
 ## Dependencies
 
 `go get -u bitbucket.org/lygo/lygo_commons`
 
 ## Dependencies
 `go get -u github.com/unidoc/unioffice/`
 `go get -u github.com/cbroglie/mustache`
 
 ### Versioning
  
  Sources are versioned using git tags:
  
  ```
  git tag v0.1.0
  git push origin v0.1.0
  ```