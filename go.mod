module bitbucket.org/lygo/lygo_ext_opendoc

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.4
	github.com/cbroglie/mustache v1.1.0
	github.com/unidoc/unioffice v1.4.0
)
